//
//  CreatePostStep1View.swift
//  DatingApp
//
//  Created by cis on 26/06/23.
//

import SwiftUI

struct CreatePostStep1View: View {
    
    let selectedColor = Color(red: 150.0/255.0, green: 108.0/255.0, blue: 251.0/255.0)
    let deselectedColor = Color(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0)
    
    @State var isPlaneActive = true
    @State var isSubwayActive = false
    @State var isPlaceActive = false
    @State var isStreetActive = false
    @State var isBusActive = false
    
    @State private var flightTxt: String = ""

    
    var body: some View {
        VStack {
            Text("Where do you want to check in?")
                .font(.largeTitle)
                .fontWeight(.semibold)
            
            HStack(spacing: 10) {
                Button {
                    deselectAll()
                    isPlaneActive.toggle()
                } label: {
                    Image("plane")
                    
                }.frame(width: 60, height: 60)
                    .background(isPlaneActive == true ? selectedColor : deselectedColor)
                    .cornerRadius(10.0)
                Button {
                    deselectAll()
                    isSubwayActive.toggle()
                } label: {
                    Image("subway")
                    
                }.frame(width: 60, height: 60)
                    .background(isSubwayActive == true ? selectedColor : deselectedColor)
                    .cornerRadius(10.0)
                Button {
                    deselectAll()
                    isPlaceActive.toggle()
                } label: {
                    Image("place")
                    
                }.frame(width: 60, height: 60)
                    .background(isPlaceActive == true ? selectedColor : deselectedColor)
                    .cornerRadius(10.0)
                Button {
                    deselectAll()
                    isStreetActive.toggle()
                } label: {
                    Image("street")
                    
                }.frame(width: 60, height: 60)
                    .background(isStreetActive == true ? selectedColor : deselectedColor)
                    .cornerRadius(10.0)
                Button {
                    deselectAll()
                    isBusActive.toggle()
                } label: {
                    Image("bus")
                    
                }.frame(width: 60, height: 60)
                    .background(isBusActive == true ? selectedColor : deselectedColor)
                    .cornerRadius(10.0)
            }
            .padding(.bottom, 30)
            Text((isPlaneActive ? "On a plane." : (isSubwayActive ? "On the subway." : (isPlaceActive ? "At a specific location." : (isStreetActive ? "On the street." : "On the bus")))))
                .font(.title)
                .fontWeight(.medium)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading, 28)
                .padding(.bottom, 20)
            
            Text((isPlaneActive ? "Enter your flight#" : (isSubwayActive ? "Enter your line" : (isPlaceActive ? "Select the place" : (isStreetActive ? "Select a street" : "Enter your line")))))
                .font(.title2)
                .fontWeight(.medium)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading, 28)
            
            TextField("DL1234, AA2345...", text: $flightTxt)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading, 28)
            

            
            Spacer()
        }
    }
    
    func deselectAll() {
        isPlaneActive = false
        isBusActive = false
        isSubwayActive = false
        isPlaceActive = false
        isStreetActive = false
    }
}

struct CreatePostStep1View_Previews: PreviewProvider {
    static var previews: some View {
        CreatePostStep1View()
    }
}
