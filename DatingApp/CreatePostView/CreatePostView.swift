//
//  ContentView.swift
//  DatingApp
//
//  Created by cis on 26/06/23.
//

import SwiftUI

struct CreatePostView: View {
    
    @State var toStep1: Bool? = false

    
    var body: some View {
        NavigationView {
            VStack {
                Text("Great. Let's get started. What do you want to do?")
                    .font(.largeTitle)
                    .fontWeight(.semibold)
                HStack {
                    Image(systemName: "checkmark.circle.fill")
                        .resizable()
                        .frame(width: 170, height: 170) // put your sizes here
                        .foregroundColor(.yellow)
                        .padding(.trailing, -40)
                    Image(systemName: "checkmark.circle.fill")
                        .resizable()
                        .frame(width: 170, height: 170) // put your sizes here
                        .foregroundColor(.green)
                }
                Spacer()
                
                
                NavigationLink(destination: CreatePostStep1View(), tag: true, selection: $toStep1) {
                    VStack {
                        Text("Post about someone")
                            .padding(.bottom, 20)
                            .fontWeight(Font.Weight.medium)
                            .foregroundColor(.black)
                        Button("Post") {
                            toStep1 = true
                        }.frame(height: 60)
                            .padding([.leading,.trailing], 140)
                            .background(Color(red: 150.0/255.0, green: 108.0/255.0, blue: 251.0/255.0))
                            .foregroundColor(.white)
                            .cornerRadius(50.0)
                            .padding(.bottom, 40)
                        
                        Text("Check in and see what happens")
                            .padding(.bottom, 20)
                            .fontWeight(Font.Weight.medium)
                            .foregroundColor(.black)
                        Button("Check in") {
                            toStep1 = true
                        }.frame(height: 60)
                            .padding([.leading,.trailing], 120)
                            .background(.black)
                            .foregroundColor(.white)
                            .cornerRadius(50.0)
                        
                        Spacer()
                    }
                   
                }.padding()
            }
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        CreatePostView()
    }
}
