//
//  DatingAppApp.swift
//  DatingApp
//
//  Created by cis on 26/06/23.
//

import SwiftUI

@main
struct DatingAppApp: App {
    var body: some Scene {
        WindowGroup {
            CreatePostView()
        }
    }
}
